package com.raep.calculonutrucion;

public class Mujeres {

    private final double sumado = 655;
    private final double xpeso = 9.56;
    private final double xtalla = 1.85;
    private final double xedad = 4.68;

    private int resultadoGER = 0;
    private int resultadoGET = 0;

    //Variables fórmula rápida
    private final double FACTORMUJER = 21.5;


    public int formulaGER (double peso,double talla,int edad){

        resultadoGER = (int)(sumado +(xpeso * peso)+(xtalla*talla)-(xedad * edad));

        return resultadoGER;

    }

    public int formulaGET (){
        double x10 = resultadoGER * 0.10;
        double x15 = resultadoGER * 0.15;

        resultadoGET = (int)(resultadoGER + x10 +x15);

        return resultadoGET;


    }

    public double GEMujer (double talla, int objetivo){
        double GE = (Math.pow(talla,2) * FACTORMUJER) * objetivo;
        return GE;
    }
}
