package com.raep.calculonutrucion;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class FormulaRapida extends AppCompatActivity {

    RadioButton hombreRadio,mujerRadio,bajarRadio,mantenerRadio;
    RadioGroup l;
    EditText talla;
    TextView showGE;

     final int BAJAR = 25;
     final int MATENER = 30;

    Hombres hombres = new Hombres();
    Mujeres mujeres = new Mujeres();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formula_rapida);
        //Permite que nuestro layout quede en orientación vertical
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        hombreRadio = (RadioButton) findViewById(R.id.radioButtonHombre);
        mujerRadio = (RadioButton) findViewById(R.id.radioButtonMujer);
        bajarRadio = (RadioButton) findViewById(R.id.radioButtonBajar);
        mantenerRadio = (RadioButton) findViewById(R.id.radioButtonMantener);
        talla = (EditText) findViewById(R.id.editTextTallas);
        showGE = (TextView) findViewById(R.id.textViewGE);

    }

    public void onclickFormulaRapida (View view){


        double tallaD = 0.0;
        double GE  = 0.0;
        String GES = " ";


        try {
            tallaD = Double.parseDouble(talla.getText().toString());
        }catch (Exception e){

            Toast.makeText(this, "Ingresa la talla en métros", Toast.LENGTH_SHORT).show();
        }

        if (hombreRadio.isChecked() == true){
            if (bajarRadio.isChecked() == true){

                GE = hombres.GEHombre(tallaD,BAJAR);

            } else if (mantenerRadio.isChecked() == true){
                GE = hombres.GEHombre(tallaD,MATENER);

            }
        }
        if (mujerRadio.isChecked() == true){

            if (bajarRadio.isChecked() == true){

                GE = mujeres.GEMujer(tallaD,BAJAR);


            } else if (mantenerRadio.isChecked() == true){
                GE = mujeres.GEMujer(tallaD,MATENER);


            }
        }
        BigDecimal bigDecimal = new BigDecimal(GE).setScale(2, RoundingMode.UP);
        GES = Double.toString( bigDecimal.doubleValue());
        showGE.setText("GE = " + GES + " kcal");
        talla.setText("");
    }
}
