package com.raep.calculonutrucion;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class HarrisBenedict extends AppCompatActivity {

    //Variables interface
    RadioButton checkhombre,checkmujer;
    EditText editpeso,edittalla,editedad;
    TextView showGER, showGET;

    //Varibles
    int GER = 0;
    int GET = 0;

    //Clases
    Hombres hombre = new Hombres ();
    Mujeres  mujer = new Mujeres ();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_harris_benedict);
        //Permite que nuestro layout quede en orientación vertical
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        editpeso = (EditText) findViewById(R.id.editTextPeso);
        edittalla = (EditText) findViewById(R.id.editTextTalla);
        editedad = (EditText) findViewById(R.id.editTextEdad);
        checkhombre = (RadioButton) findViewById(R.id.radioButtonHombre);
        checkmujer = (RadioButton) findViewById(R.id.radioButtonMujer);
        showGER = (TextView)  findViewById(R.id.textViewGER);
        showGET = (TextView)  findViewById(R.id.textViewGET);
    }

    public void onClic (View view){

        String pesoS = "";
        double peso = 0.0;
        String tallaS = "";
        double talla = 0.0;
        String edadS = "";
        int edad = 0;
        String GERS = "";
        String GETS = "";

        try {
            pesoS = editpeso.getText().toString();
            peso = Double.parseDouble(pesoS);
            tallaS = edittalla.getText().toString();
            talla = Double.parseDouble(tallaS);
            edadS = editedad.getText().toString();
            edad = Integer.parseInt(edadS);
        }catch (Exception e){
            Toast.makeText(this, "Ingresa todos los datos.", Toast.LENGTH_SHORT).show();
        }

        if (checkhombre.isChecked() == true){

            GER = hombre.formulaGER(peso,talla,edad);
            GERS  = Integer.toString(GER);
            GET = hombre.formulaGET();
            GETS = Integer.toString(GET);

        }

        if (checkmujer.isChecked() == true){

            GER = mujer.formulaGER(peso,talla,edad);
            GERS  = Integer.toString(GER);
            GET = mujer.formulaGET();
            GETS = Integer.toString(GET);

        }

        showGER.setText("GER = " + GERS + " kcal");
        showGET.setText("GET = " + GETS + " kcal");

        editpeso.setText("");
        editedad.setText("");
        edittalla.setText("");

    }


}
