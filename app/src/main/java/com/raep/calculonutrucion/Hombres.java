package com.raep.calculonutrucion;

public class Hombres {

    //Varibles Harris Benedict
    private final double sumado = 66.5;
    private final double xpeso = 13.75;
    private final int xtalla = 5;
    private final double xedad = 6.78;
    private int resultadoGER = 0;
    private int resultadoGET = 0;

    //Varibles fórmula rápida

    private final int FACTORHOMBRE = 23;


    public int formulaGER (double peso,double talla,int edad){

        resultadoGER = (int)(sumado +(xpeso * peso)+(xtalla*talla)-(xedad * edad));

        return resultadoGER;

    }

    public int formulaGET (){

        double x10 = resultadoGER * 0.10;
        double x15 = resultadoGER * 0.15;

        resultadoGET = (int)(resultadoGER + x10 +x15);

        return resultadoGET;

    }

    //Métodos de fórmula raṕida

    public double GEHombre ( double talla, int objetivo){

        double GE = (Math.pow(talla,2) * FACTORHOMBRE) * objetivo;
        return GE;
    }


}
